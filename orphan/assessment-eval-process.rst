.. _assessment-eval-process:

Project & Team Lifecycle
========================

**For projects hosted through the OSC’s sponsored professional services
work, a lifecycle exists to describe the process of onboarding those
projects into the community. Existing and more mature open source
projects may be “fast tracked” through this lifecycle, on a case by case
basis.**

The term "sub-project" or "project" within the OSC will refer to a
collaborative effort to deliver a work item. There may be some projects
that are intended to produce a document, such as a requirements or use
cases document, a whitepaper, or analysis. Others may be to develop a
new capability, or refactor (or remove) an existing capability for
community technology releases. Such projects may take the form of a new
component (e.g. a new repository) or may propose additions, deletions or
changes to an existing repository(s).

Similarly, a "team" exists to work on an ongoing strategic initiative
within the community, such as documentation (generally), diversity &
inclusion, or education & training.

Many other free & open source software communities leverage an
incubation process for new work items, and this seems to have a desired
effect of encouraging new ideas and tracks of work, while at the same
time providing clear guidance to the broader community as to what is
real and supported, versus what is still in the
exploratory/experimental/developmental phases.

Therefore the OSC has adopted a similar lifecycle process. Projects and
teams within are in one of five possible phase: Proposal, Incubation,
Active, Deprecated, and End of Life.

Projects may not necessarily move through those states in a linear way
and may go through several iterations.

Proposal
--------

Proposal for projects or teams shall be submitted to the Technical
Steering Committee (TSC) for review, using a Proposal Template, as
described in Appendix A. Proposals that are approved shall generally
proceed into the Incubation phase. If a proposal recommends changes to
an existing project or team, that proposal will be turned over to the
relevant project/team maintainer(s) to handle as they deem fit, in
consultation with the creator(s) of the proposal.

A Proposal must:

-  have a clear description
-  have a well-defined scope
-  identify committed development resources
-  identify initial maintainers
-  be vendor neutral

Incubation
----------

Approved proposals for projects or teams next enter into the Incubation
phase. For new software projects, a repository & issue tracker will be
created under the OSC forge. Should a project be created that intends to
work on an existing OSC project code base, new features/capabilities
should be handled through pull requests labeled with tags that identify
the name of the incubation project and tag it as “incubator”. Ideally,
any such new features should be capable of being enabled/disabled with
feature-flags.

Projects in Incubation may overlap in functionality with one another.
Entering Incubation is meant to be fairly easy to allow for community
exploration of different ideas. Entering this phase does not guarantee
that the project will eventually achieve the Active phase -- a projects
may never reach the next phase due to lack of demand, lack of
contributors, or for technical reasons.

In order to exit the Incubation phase and advance to the Active phase,
several specific criteria should be met. See Appendix B, "Incubation
Advancement Criteria". Once the project has met these criteria, its
maintainer(s) can then decide to request a "advancement review" from the
Technical Steering Committee (TSC).

Generally, projects seeking to advance from the Incubation phase must:

1. Have fully functional code base,
2. Have test coverage commensurate with other Active projects,
3. Have an active and diverse community of developers (who, if supported
   by an external organization, do not all have affiliation with any one
   organization), and
4. Have a history of releases that follow the Active release process.

Active
------

Projects that have successfully exited the Incubation phase are in the
Active phase.

Anyone within the OSC may propose that a project be deprecated, by
submitting a rationale and identifying a substitute project, if any. The
maintainer(s) of the project shall vote on such a request, and if it
passes, forward that recommendation to the Technical Steering Committee
(TSC). Should the maintainer(s) of said project no longer be active or
available within the community, the request shall immediately be
forwarded to the TSC Members of the community that disagree with the
request may make their case to the TSC while the request is under
consideration. The TSC shall consider all points of view, and render a
final decision to deprecate or not.

Deprecated
----------

A Deprecated project should be maintained for a six month period by its
project contributors, after which time it will no longer produce any
subsequent software releases. Notice will be given to the public of the
project's deprecation. After the six-month deprecation period, the
project will be labeled End of Life.

End of Life
-----------

A project that is no longer actively developed or maintained by the OSC.

Appendix A: Proposal Template
-----------------------------

A OSC Sub-Project (SP) is any initiative that furthers the OSC's goals,
including a project that is a new solution to a problem, A SP represents
any new effort that requires resources and commitment from the
community.

The SP proposal template ushers originators, designers, implementers,
testers of a project through community and TSC approval. The proposal
should evolve organically as the SP moves through the stages of
conception, design, approval, implementation, deployment, and
maintenance. The project and hence the proposal may also need several
iterations. In its terminal forms it can also serve as a short manual to
the features of the project for users.

This proposal template is descriptive and not normative, a guide rather
than the law. Prior templates like the internet RFC process, Bitcoin
Improvement Proposal, Python Improvement Proposal etc. were used as
guidelines to develop this template.

The earliest conception of a new project must be vetted in the OSC
public forums before creating a project proposal. The project should
have one or more technical champions who believe in the project and who
will serve as the maintainers of the project. The technical champions
can change in the middle of the project.

Please note that readability is very important. The language of the
proposal should be English if possible as that is the lingua franca of
the OSC. This is extremely important as a clear statement of the problem
and its technical details are helpful to coalesce the community around a
solution and prompt volunteers. The outline of a project is given below
with comments on the sections.

1.  **Project name/identifier:** A code/project/"brand" name for the
    project and a short description.
2.  **Sponsor:** Name and contact details.
3.  **Abstract:** Approximately 50-word description of the project.
4.  **Context:** Which other projects, if any, is this project derived
    from? What other projects, if any, is it related to?
5.  **Motivation:** A longer justification of the SP, around 200-300
    words. Why is this project better at solving a problem compared to
    parallel proposals or implemented projects?
6.  **Status:** Proposal - Incubation - Active - Deprecated - End of
    Life
7.  **Community Feedback:** Address any possible objections and also
    support that came up during seed proposal from technical community
    in the forums.
8.  **Solution:** Your proposed approach to alleviating the problem
    addressed in the motivation section above. Try to make this as
    detailed as possible. The topics given below are just suggestions,
    address only if they are relevant to your problem:
9.  Transactions including types, confidentiality, signing,
    traceability, identity of participants, contracts, scripts, etc.
10. Effects on user-facing clients that help with transaction formation.
11. Effects on the network, throughput, visibility to other
    participants, changes in standards/protools (if any), criteria for
    network participation, etc.
12. Backward compatibility evaluation.
13. Rough design and scenarios on the probable effects, if any.
14. Traceability & testing criteria to gauge effects of project on
    problem area.
15. Proposed license of project codebase, including license of any
    dependencies.
16. Evaluation of any trademarks used in the project name or codebase.
17. Estimation of effort and resources needed to launch & maintain
    project, along with timeline of development.
18. Description of how the software/project would be hosted/tested by a
    user, how to deploy and use, how verify it works correctly.
19. References and citations, if any.
20. Success criteria: How do we know that the project is successful?
    Suggest specific & measurable criteria if possible. Make references
    to successor projects that this project may enable, if any.
