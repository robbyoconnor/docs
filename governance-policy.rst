.. _governance-policy:

OSC Governance Policy
=====================

This policy came into effect in December 2016 and will be reviewed periodically (see revision sections). The last modification has been made in May 2017.

:ref:`Read more <osc-community>` about the specific governance structures within the OSC Community.

Goals
-----

The goals of governance in the Open Source Center (OSC) are to:

-  Create a set of minimum requirements for sub-projects hosted within the organization,
-  Create a lightweight project lifecycle that:
-  leads each sub-project to articulate its goals and how to achieve them,
-  encourages desired behaviors (e.g., open development),
-  provides motivation for the project to succeed,
-  leads to non-viable projects failing quickly,
-  provides opportunities for other community members, and
-  avoids bureaucracy through lightweight & flexible approaches.
-  Encourage in-scope projects related to our mission, values, and vision to be leverage services within the organization rather than going elsewhere, and
-  Set clear expectations to vendors, upstream and downstream projects, and participating community members.

Principles
----------

Openness
~~~~~~~~

The Open Source Center is open to all individuals in their individual capacities, and provides the same opportunity to all. Everyone participates with the same rules. There are no rules to exclude prima facie any potential individual contributors, which include individuals associated with direct competitors in the marketplace.

Transparency
~~~~~~~~~~~~

All project discussions, minutes, deliberations, project plans, designs, plans for new features, and other artifacts shall be open, public, searchable, and easily accessible to everyone.

Equity of Ideas
~~~~~~~~~~~~~~~

The OSC strives toward an equitable community of diverse ideas. Our projects and our leaders are dedicated to creating the best software, and we believe that happens when everyone feels empowered to contribute both their code and their thoughts to our community. Responsibility in the OSC is earned through trust, and increasing trust between all of our community participants is based upon our projects and work being as inclusive as possible.

Member Project & Team Roles
---------------------------

The Open Source Center organizes itself into a number of Member Projects, which follow the Project Governance and Project Lifecycle processes as outlined in this document. SPs are run by individuals and may be referred to as “teams” to highlight the collaborative nature of development. For example, each project has a team page on the OSC web site.

Project/Team Leader
~~~~~~~~~~~~~~~~~~~

OSC member projects and teams are managed by a project or team leader, who is also a committer of the project/team they lead. The Project Leader is the public figurehead of the project and is responsible for the health of the project. Due to their status in the community, project leads can also act as referees should disagreements amongst committers of the project arise. The project lead typically also has write access to resources, such as the web page of a specific project.

Maintainer
~~~~~~~~~~

For larger or more complex projects, maintainers may one or several components within one or more member projects. A maintainer reviews and approves changes that affect their components. It is a maintainer's prime responsibility to review, comment on, co-ordinate and accept patches from community members, and to maintain the design cohesion of their components. Each member project maintainer shall be listed in a MAINTAINERS file in the root of that project's code repository or documentation page.

Committers
~~~~~~~~~~

Committers are Maintainers who are allowed to commit changes into a repository. The committer acts on the wishes of the maintainers and applies changes that have been approved by the respective maintainer(s) to the repository. Due to their status in the community, committers can also act as referees, should disagreements amongst maintainers arise. Committers are listed on the SP’s team web page.

Mentor
~~~~~~

Younger member projects may optionally have a desire for a mentor to help ensure that the sub-project will be successful. Mentors can be maintainers, project leaders, advisory board members or other distinguished OSC community members. Mentors are expected to have a monthly review with their mentored projects.

Sponsor
~~~~~~~

To form a new mentored project or team, the community requires a sponsor to support the creation of the new project. A sponsor can be a project leader or committer of an active project, a member of the Steering Committee, or the director of community. This ensures that a distinguished community member supports the idea behind the project and is responsible to find an appropriate mentor for the project.

Making Contributions
--------------------

Making contributions to member projects in the Open Source Center follows the conventions as they are known in the Linux Kernel community. In summary, contributions are made through pull requests that are reviewed by the community. **The Open Source Center does not require community members to sign copyright assignments or license agreements.** However, it does require all contributors to provide an assent that they have proper legal authority to license their contributions to the Open Source Center in their individual capacity under a compatible license.

This is not an assignment of copyright; the DCO simply states that the code you submitted is yours to contribute, is unencumbered to the best of your knowledge, and that you are free to submit it without any restrictions beyond the license under which you have submitted the code. Some employers or academic institution claim ownership over code that is written in certain circumstances, so please do due diligence to ensure that you have the right to submit the code. This helps our users ensure the software we license to them is free and clear of any legal
encumbrances.

Contribution guidelines for specific sub-projects will be found in those projects’ code repositories in the CONTRIBUTING file, or on the Open Source Center web site.

OSC Developer Certificate of Origin 1.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By making a contribution to this project, I certify that:

a) The contribution was created in whole or in part by me and I have the right to submit it under the open source license indicated in the file; or

b) The contribution is based upon previous work that, to the best of my knowledge, is covered under an appropriate open source license and I have the right under that license to submit that work with modifications, whether created in whole or in part by me, under the same open source license (unless I am permitted to submit under a different license), as indicated in the file; or

c) The contribution was provided directly to me by some other person who certified (a), (b) or (c) and I have not modified it; and

d) In the case of each of (a), (b), or (c), I understand and agree that this project and the contribution are public and that a record of the contribution (including all personal information I submit with it, including my sign-off) is maintained indefinitely and may be redistributed consistent with this project or the open source license indicated in the file.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
    
