.. _self-assessment:

===============================
OSC Project Membership Proposal
===============================

*************************
Motivation for Membership
*************************

-  **Point of Contact:** Who should be the primary point of contact as your project is evaluated by the OSC GAB? Provide a name, affiliations (if any), email address, and primary time zone.
-  **Rationale:** Why does your project want to join the OSC? Specifically, what benefits do you expect to take advantage of immediately and within a few years?
-  **Alternatives:** OSC does encourage projects to apply to evaluate different service & hosting strategies to find the best fit. Does your project have an application pending with any other similar organization? What do you see as the pros and cons of the various organizations you've applied to?
-  **Sponsor:** Please provide a name and contact details of one or more people in the OSC community with whom you have discussed your project and its potential participation.
-  **Community Feedback:** Describe any specific areas of concern (and areas of support) that were raised during preliminary discussions about joining the OSC. These could include community, governance, legal, or technical concerns, among any others.

****************
Project Overview
****************

-  **Description:** Please give a detailed description of the project, including relevant URLs, target platforms, package availability, SaaS offerings, etc.
-  **Status:** Which lifecycle phase would you consider your project? Examples include: Idea/Proposal, Startup, Active, Deprecated, End-of-Life. Explain why you chose the phase you did.
-  **Competition:** Why is this project better at solving a problem compared to other “competing” projects attempting to address similar problems?
-  **Context:** Which other projects, if any, is this project derived from? What other projects, if any, is it related to?

*****************************
Legal & Intellectual Property
*****************************

-  **Licenses:** What FLOSS License(s) does your project use? Please include the primary license, and list other licenses for code that is included. (e.g., "The project as a whole is GPLv3-or-later, but about a dozen files in the directory src/external/ are under the Apache-2.0 license"). Please be sure to include information on documentation licensing as well as software licensing.
-  **Trademarks:** Who currently holds your projects' trademarks, if any? (Consider both registered trademarks and those built through common use.) When was your project’s name first used, and who used it?
-  **Logo:** Does your project have a logo? If so, who drew it, when did they draw it, where is it displayed and what is its license? -  **Patents:** Are you aware of anyone in your project, individual or company, holding a patent in any jurisdiction that are in any way related to your project?
-  **Disputes:** Has your project ever had legal trouble, been involved in legal proceedings or received a letter accusing your project of patent, copyright, trademark or other types of infringement?

*********
Financial
*********

-  **Accounting:** Have you ever had funds held by the project, or by any individual on behalf of the project? How and for what did you spend those funds? Are there funds remaining? If so, who is holding them now?
-  **Fundraising:** Do you have any ongoing fundraising programs for your project? How do they operate, and how much funding is brought in through these mechanisms currently? Where do you expect most of your donor base to be geographically?
-  **Expenses:** Going forward, once inside the OSC, how do you expect to spend funds that you raise? What types of activity do you want to ask the OSC to lead on your behalf? Where geographically do you want those activities to take place?
-  **Co-Promotion:** Is your project able and willing to participate in fundraising campaigns with the OSC on an annual or perhaps more frequent basis?
-  **Debt:** Does your project owe funds to anyone?

*********
Community
*********

-  **History:** Please give a brief history of the project, focusing on how the community developed and the general health of the community. Be sure to include information on any forks or other disputes that have occurred in the community.
-  **Governance:** Please explain how your project is governed. Who makes the decisions in the project? How do you resolve disputes, particularly about non-code issues?
-  **Affiliations & Partnerships:** Does your project have any existing for-profit or non-profit affiliations, funding relationships, or other agreements between the project and/or key leaders of your project and other organizations? Has the project had such affiliations in the past? Please list of all of them in detail and explain their nature. Even tangential affiliations and relationships, or potential affiliations that you plan to create should be included.
-  **Users:** Approximately how many users does your project have, and what items lead you to believe your user base is of a particular size (e.g., post counts to your user mailing list, survey results, etc.)?
-  **Contributors:** Please list the names, email addresses, and affiliations (e.g., employer) of key developers and major contributors. Include both current and past contributors and developers. Please include date ranges of when those developers/contributors were active. Please make this list as extensive and complete as possible. You need not include every last person who sent one patch, but please include at least those who regularly sent patches or were/are regular contributors. If your project has contributors who have been inactive for more than five years, you need only to list such inactive contributors if they made substantial contributions.
-  **Other Information:** Please include any other pertinent information not given above that you feel we should review with your application.

**********************
Technical Backgrounder
**********************

As part of our assessment, we may want to explore more deeply your project’s technical architecture & solution design. To assist, please several detailed paragraphs discussing how your project attempts to alleviate the problems described in the earlier sections. The following suggested discussion points may help you get started. Dependending on relevancy, you need not describe all of these items, nor limit your discussion to them. Consider:

-  Transactions including types, confidentiality, signing, traceability, identity of participants, contracts, scripts, etc.
-  Effects on user-facing clients that help with transaction formation.
-  Effects on the network, throughput, visibility to other participants, changes in standards/protools (if any), criteria for network participation, etc.
-  Backward compatibility evaluation.
-  Rough design and scenarios on the probable effects, if any.
-  Traceability & testing criteria to gauge effects of project on problem area.
-  Proposed license of project codebase, including license of any dependencies.
-  Evaluation of any trademarks used in the project name or codebase.
-  Estimation of effort and resources needed to launch & maintain project, along with timeline of development.
-  Description of how the software/project would be hosted/tested by a user, how to deploy and use, how verify it works correctly.
-  References and citations, if any.
-  Success criteria: How do we know that the project is successful? Suggest specific & measurable criteria if possible. Make references to successor projects that this project may enable, if any.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
    
