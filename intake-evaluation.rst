.. _intake-evaluation:

###############################
OSC Project Maturity Evaluation
###############################

********
Overview
********

The goal of this maturity model is to describe how OSC projects operate, in a concise and high-level way.  It is used as one of the evaluation tools for OSC membership, service, and grant-making decisions. Most importantly, it's used to describe where individual open source projects in the OSC are on the road to becoming more mature and adopting best practices.

It is meant to be usable outside of the OSC as well, for projects that might want to adopt some or all of these principles. Projects that envision becoming an OSC member might start working towards this to prepare for their move.

It does not describe all the details of how our projects operate, but aims to capture the best practices of how succesful & mature open source projects operate, and point to additional information where needed. To keep the main model as concise as possible we use footnotes for anything that's not part of the core model.

Contrary to other maturity models, we do not define staged partial compliance levels. A mature project complies with all the elements of this model, and other projects are welcome to pick and choose the elements that suit their goals.

Note that we try to avoid using the word "must" below. The model describes the state of a mature project, as opposed to a set of rules.

**************
Maturity Model
**************

Each item in the model has a unique ID to allow them to be easily referenced elsewhere.

Software Code
=============

* **CD10** The project produces Open Source software, for distribution to the public at no charge. [1]_

* **CD20** The project's code is easily discoverable and publicly accessible.

* **CD30** The code can be built in a reproducible way using widely available standard tools.

* **CD40** The full history of the project's code is available via a source code control system, in a way that allows any released version to be recreated.

* **CD50** The provenance of each line of code is established via the source code control system, in a reliable way based on strong authentication of the committer. When third-party contributions are committed, commit messages provide reliable information about the code provenance. [2]_

* **CD60** The code contains README, NOTICE, and CONTRIBUTING files (or README sections).

Licenses and Copyright
======================

* **LC10** The code is released under one of the preferred copyleft licenses explained in our :ref:`Licensing Principles <membership-details>`.

* **LC20** Libraries that are mandatory dependencies of the project's code do not create more restrictions than the project's license does. [3]_ [4]_

* **LC30** The libraries mentioned in LC20 are available as Open Source software.

* **LC40** Committers are bound by an Individual Contributor Agreement (the `"Apache iCLA" <http://www.apache.org/licenses/icla.txt>`_ being an example) that defines which code they are allowed to commit and how they need to identify code that is not their own.

* **LC50** The copyright ownership of everything that the project produces is clearly defined and documented. [5]_

* **LC60** The project name has been checked for trademark issues.

Software Releases
=================

* **RE10** Releases consist of source code, distributed using standard and open archive formats that are expected to stay readable in the long term. [6]_

* **RE30** Releases are signed and/or distributed along with digests that can be reliably used to validate the downloaded archives.

* **RE40** Convenience binaries can be distributed alongside source code but they are not official releases -- they are just a convenience provided with no guarantee.

* **RE50** The release process is documented and repeatable to the extent that someone new to the project is able to independently generate the complete set of artifacts required for a release.

* **RE60** Release plans are developed and executed in public by the community, and approved by the project's governing body.

* **RE70** The project should use the OSC standard release taxonomy, once that is agreed upon.

* **RE80** The project has released at least one version.

Software Quality
================

* **QU10** The project is open and honest about the quality of its code. Various levels of quality and maturity for various modules are natural and acceptable as long as they are clearly communicated.

* **QU20** The project puts a very high priority on producing secure software. [7]_

* **QU30** The project provides a well-documented channel to report security issues, along with a documented way of responding to them. [8]_

* **QU40** The project puts a high priority on backwards compatibility and aims to document any incompatible changes and provide tools and documentation to help users transition to new features.

* **QU50** The project strives to respond to documented bug reports in a timely manner.

* **QU60** The project must include a unit and integration test suite of sufficient [13]_ coverage, and must document its coverage. Additional performance and scale test capability is desirable.

* **QU70** The project must including enough documentation for anyone to test or deploy any of the software.

* **QU80** The project must document how it integrates with other OSC Member (or external) projects. Where applicable, the project should be compatible with other active projects.

* **QU90** The project has set up a Continuous Integration pipeline for testing and deployment purposes.

* **QU100** The project must demonstrate sufficient scalability and document its scalability over various dimensions appropriate to the project.

Community
=========

* **CO10** The project has a well-known homepage that points to all the information required to operate according to this maturity model.

* **CO20** The community welcomes contributions from anyone who acts in good faith and in a respectful manner and adds value to the project.

* **CO30** Contributions include not only source code, but also documentation, constructive bug reports, constructive discussions, marketing and generally anything that adds value to the project.

* **CO40** The community is a "holarchy" (see :ref:`Governance Principles <membership-details>`) and over time aims to give more rights and responsibilities to contributors who add value to the project.

* **CO50** The way in which contributors can be granted more rights such as commit access or decision power is clearly documented and is the same for all contributors.

* **CO60** The community operates based on consensus of its members (see CS10) who have decision power. Dictators, benevolent or not, are not welcome in Apache projects.

* **CO70** The project strives to answer user questions in a timely manner.

* **CO80** The project has an active and diverse set of contributing members representing various constituencies.

Consensus Building
==================

* **CS10** The project maintains a public list of its contributors who have decision power and some sort of documented governance process.

* **CS20** Decisions are made by consensus among PMC members [9]_ and are documented on the project's main communications channel. Community opinions are taken into account but the PMC has the final word if needed.

* **CS30** Documented voting rules are used to build consensus when discussion is not sufficient. [10]_

* **CS40** In Apache projects, vetoes are only valid for code commits and are justified by a technical explanation, as per the Apache voting rules defined in CS30.

* **CS50** All "important" discussions happen asynchronously in written form on the project's main communications channel. Offline, face-to-face or private discussions [11]_ that affect the project are also documented on that channel.

Independence
============

* **IN10** The project is independent from any corporate or organizational influence. [12]_

* **IN20** Contributors act as themselves as opposed to representatives of a corporation or organization.

* **IN30** The project is not highly dependent on any single contributor. There are at least 3 legally independent contributors (e.g., code committers), and there is no single organization that is vital to the success of the project.

Impact
======

* **IM10** The project should be used in real applications and not just in demos. Because not all real-world implementations may be inspected publicly, in such cases statements providing as much details as possible about these implementations should be made.

* **IM20** The project should be able to clearly make the case for its importance in the Development and/or Humanitarian sector(s).

*******************
About This Document
*******************

The work "DIAL Open Source Center Project Maturity Evaluation" was adapted from "Apache Project Maturity Model" by the Apache Software Foundation, licensed under the Apache License version 2.0.

.. [1] "For distribution to the public at no charge" is straight from the from the ASF Bylaws at http://apache.org/foundation/bylaws.html.
.. [2] See also LC40.
.. [3] It's ok for platforms (like a runtime used to execute our code) to have different licenses as long as they don't impose reciprocal licensing on what we are distributing.
.. [4] http://apache.org/legal/resolved.html has information about acceptable licenses for third-party dependencies
.. [5] In Apache projects, the ASF owns the copyright for the collective work, i.e. the project's releases. Contributors retain copyright on their contributions but grant the ASF a perpetual copyright license for them.
.. [6] See http://www.apache.org/dev/release.html for more info on Apache releases
.. [7] The required level of security depends on the software's intended uses, of course. Expectations should be clearly documented.
.. [8] Apache projects can just point to http://www.apache.org/security/ or use their own security contacts page, which should also point to that.
.. [9] In Apache projects, "consensus" means widespread agreement among people who have decision power. It does not necessarily mean "unanimity".
.. [10] For Apache projects, http://www.apache.org/foundation/voting.html defines the voting rules.
.. [11] OSC projects may have a private mailing list that their governing body is expected to use only when really needed. The private list is typically used for discussions about people, for example to discuss and to vote on PMC candidates privately.
.. [12] Independence can be understood as basing the project's decisions on the open discussions that happen on the project's main communications channel, with no hidden agendas.
.. [13] "Sufficient" can mean different things for different projects, the importance here is that the community can be honest and open about the rationale for choosing what to cover and what not to cover, and about what needs test coverage but doesn't have it.

.. _`Apache Project Maturity Model`:  https://community.apache.org/apache-way/apache-project-maturity-model.html

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
    
