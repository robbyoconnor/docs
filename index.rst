Welcome to the DIAL Open Source Center!
***************************************

Who We Are
==========

The mission of the DIAL Open Source Center (OSC) is to convene a vibrant, inclusive, free & open source software community that promotes knowledge sharing, collaboration, and co-investment in technology & human capacity to support positive social change in communities around the world. Together, our community will learn and demonstrate how to overcome key barriers in conceptualizing, designing, and building these software products, collectively building a public commons that can be scaled up to achieve global value.

We aim to achieve this mission in the following 4 ways:

Building Membership
-------------------

Our members strive to become increasingly mature software projects focusing on impact; agree to a common set of values, mission, and vision; and follow guiding philosophies based on the Principles for Digital Development & other engineering and open source best practices. :ref:`Read more about these principles <membership-details>` and how to become a member!

Service Delivery
----------------

Members are able to apply for services provisioned by and for the OSC Community along with its service delivery partners. :ref:`Read more about these shared services and how to apply. <services>`

Direct Funding
--------------

The OSC Community will periodically make catalytic grants available directly to its members for product development in areas that are traditionally difficult to raise funding for within the confines of a particular project's scope.  :ref:`Read more about how to apply<grants>` for a grant.

Community Building
------------------

We advocate for projects, funders, NGOs and donors to adopt the PDDs and avoid funding fragmentation and duplication of efforts, specifically by driving the formation of a governance structure for the T4D community as a whole.  :ref:`Read more about our governance structure and our leadership team <osc-community>`.

Program Governance
==================

More detailed policy descriptions about the DIAL Open Source Center can be found here:

* :ref:`code-of-conduct`
* :ref:`contribution-policy`
* :ref:`copyright-policy`
* :ref:`governance-policy`
* :ref:`privacy-policy`
* :ref:`trademark-policy`
* :ref:`decision-making`

About this documentation
========================

The DIAL Open Source Center (OSC) serves to provide support to open source platforms that identified as public goods in the development sector. This site is a library of the program's working documents.

Additional documents will be published here as they are developed, and all documents are subject to change and evolve over time as we receive feedback from our partners and participants. This documentation site is updated in real-time from our `GitLab document repository`_, and pull requests to improve these documents are always welcome. See that code repository's `contributing`_ file for details.

.. _`GitLab document repository`: https://gitlab.com/dial/open-source-center/tree/master/docs

.. _`contributing`: https://gitlab.com/dial/osc/docs/blob/toc-refactor/CONTRIBUTING.md

.. _about-osc:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: About the Open Source Center

   membership-details
   osc-community

.. _participant-services:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Member Services

   Shared Services Overview <services>
   Service Details <services-details>
   grants
   Google Summer of Code <soc>
   GSoC Sub-Org Template <soc-template-suborg>
   GSoC Student Template <soc-template-student>

.. _getting-started:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Member Resources

   Project Self-Assessment <intake-services>
   Proposal Template <self-assessment>
   Project Maturity Evaluation <intake-evaluation>

.. _program-governance:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Governance Resources

   OSC Code of Conduct <code-of-conduct>
   governance-policy
   decision-making
   contribution-policy
   copyright-policy
   privacy-policy
   trademark-policy

