.. _intake-services:

###########################
OSC Project Self-Assessment
###########################

One of the benefits of participating in the DIAL Open Source Center is to bring a higher level of maturity and impact to your T4D open source project. This  self-assessment is designed to start your thinking process about what you may  be doing well currently, and potential areas to focus on.

Participation in the OSC can help you with some or all of these areas! You need not feel guilty about not having some or many areas "under control". And some  may not be relevant to your project.

Once you've gone through this self-assessment exercise with your fellow project maintainers (if any), your next step is to prepare for the OSC Membership  Proposal process.

**************************
Potential needs & services
**************************

For each of the items below, mark whether your project currently has effort  ongoing, if you want to do so, or if you don't think that item is necessary for  your project.

+--------------------------------------------------+------+------+------------+
|                                                  | Have | Want | Don't Need |
+==================================================+======+======+============+
| Product strategy                                 |      |      |            |
+--------------------------------------------------+------+------+------------+
| Product vision/roadmap                           |      |      |            |
+--------------------------------------------------+------+------+------------+
| Product Management/Customer Engagement           |      |      |            |
+--------------------------------------------------+------+------+------------+
| Product Coordination with T4D Community          |      |      |            |
+--------------------------------------------------+------+------+------------+
| Architecture/High-level Design                   |      |      |            |
+--------------------------------------------------+------+------+------------+
| Agile/Process Management                         |      |      |            |
+--------------------------------------------------+------+------+------------+
| Contributor/backlog management                   |      |      |            |
+--------------------------------------------------+------+------+------------+
| Software development                             |      |      |            |
+--------------------------------------------------+------+------+------------+
| Mentoring                                        |      |      |            |
+--------------------------------------------------+------+------+------------+
| Advisory board operation                         |      |      |            |
+--------------------------------------------------+------+------+------------+
| Marketing program organization                   |      |      |            |
+--------------------------------------------------+------+------+------------+
| Volunteer matchmaking/management                 |      |      |            |
+--------------------------------------------------+------+------+------------+
| Conflict arbitration                             |      |      |            |
+--------------------------------------------------+------+------+------------+
| Community outreach                               |      |      |            |
+--------------------------------------------------+------+------+------------+
| Community policy consultation                    |      |      |            |
+--------------------------------------------------+------+------+------------+
| Incubation process management                    |      |      |            |
+--------------------------------------------------+------+------+------------+
| External communications                          |      |      |            |
+--------------------------------------------------+------+------+------------+
| Corporate compliance                             |      |      |            |
+--------------------------------------------------+------+------+------------+
| Financial Services                               |      |      |            |
+--------------------------------------------------+------+------+------------+
| Business development                             |      |      |            |
+--------------------------------------------------+------+------+------------+
| Fundraising                                      |      |      |            |
+--------------------------------------------------+------+------+------------+
| Marketing/advertising                            |      |      |            |
+--------------------------------------------------+------+------+------------+

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
    
