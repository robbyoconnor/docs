.. _decision-making:

Decision Making Process: Recommendations
========================================

Consensus Decision Making
-------------------------

Open Source Center projects or teams are self-governing and driven by the people who volunteer for the job. When more formal decision making and coordination is required, decisions are taken with a "lazy consensus" approach: a few positive votes with no negative vote is enough to get going.

Sometimes a member of the community will believe a specific action is the correct one for the community but are not sure enough to proceed with the work under the lazy consensus model. In these circumstances they can state Lazy Consensus is in operation.

What this means is that they make a proposal and state that they will start implementing it in 72 hours unless someone objects. 72 hours is chosen because it accounts for different time zones and outside commitments. If the 72 hours are touching a weekend/holidays it would be wise to extend the timeframe a bit. This will ensure that people can participate in the proposal even when they were offline over the weekend.

Voting is done with numbers:

-  +1 : a positive vote
-  0 : abstain, have no opinion
-  -1 : a negative vote

A negative vote should include an alternative proposal or a detailed explanation of the reasons for the negative vote. The project community then tries to gather consensus on an alternative proposal that resolves the issue. In the great majority of cases, the concerns leading to the negative vote can be addressed.

Conflict Resolution
-------------------

Refereeing
~~~~~~~~~~

Open Source Center (OSC) projects and teams strive for equity of ideas leveraging aspects of both democracy & meritocracy. In situations where there is disagreement on issues related to the day-to-day running of the project, Committers and Project Leads are expected to act as referees and make a decision on behalf of the OSC. Referees should consider whether making a decision may be divisive and damaging for the OSC. In such cases, the committers of the project can privately vote on an issue, giving the decision more weight.

Last Resort
~~~~~~~~~~~

In some rare cases, the lazy consensus approach may lead to the community being paralyzed. Thus, as a last resort when consensus cannot be achieved on a question internal to a project, the final decision will be made by a secret ballot majority vote amongst the committers and project lead. If the vote is tied, the project lead gets an extra vote to break the tie.

For questions that affect several projects, committers and project leads of mature projects will hold a secret ballot majority vote. If the vote is tied, the Technical Steering Committee (TSC) or Governance Advisory Board (GAB) will break the tie through a casting vote.

Elections and Formal Votes
--------------------------

Maintainer Elections
~~~~~~~~~~~~~~~~~~~~

Developers who have earned the trust of maintainers and the project lead can be promoted to Maintainer. A two-stage mechanism is used: Nomination: A maintainer should nominate himself by proposing a patch to the MAINTAINERS file or posting a nomination to the project's category on the Open Source Center forums. Alternatively, another maintainer may nominate a community member. A nomination should explain the contributions of proposed maintainer to the project as well as a scope (set of owned components). Where the case is not obvious, evidence such as specific patches and other evidence supporting the nomination should be cited. Confirmation: Normally, there is no need for a direct election to confirm a new maintainer. Discussion should happen in the forums using the principles of consensus decision making. If there is disagreement or doubt, the project lead or a committer should ask the Director of Community to arrange a more formal vote. Committer Elections Developers who have earned the trust of committers in their sub-project can through election be promoted to Committer. A two-stage mechanism is used:

1. **Nomination:** Community members should nominate candidates by posting a proposal by sending a direct message to the appropriate category on the Open Source Center (OSC) forums, explaining the candidate's contributions to the sub-project and thus why they should be elected to become a Committer of that sub-project. The nomination should cite evidence such as patches and other contributions where the case is not obvious. Existing Committers will review all proposals, check whether the nominee would be willing to accept the nomination and publish suitable nominations in the OSC forums publicly for wider community input.
2. **Election:** A committer will be elected using the decision making process outlined earlier. Voting will be done by committers for that project privately using a voting form that is created by the community manager. Should there be a negative vote the Project Leader and Director of Community will try and resolve the situation and reach consensus. Results will be published in the public forums.

Project Leader Elections
~~~~~~~~~~~~~~~~~~~~~~~~

Projects which lose their project leader are at risk of failing. Should this occur, the project's maintainer community should agree on a suitable new project leader and follow the same election process as outlined above.

Formal Votes
~~~~~~~~~~~~

Sometimes it is necessary to conduct formal voting within the Open Source Center (outside of elections). Formal votes are necessary when processes and procedures are introduced or changed, or as part of the Project Governance. Who is eligible to vote, depends on whether the scope of a process or procedure is local to a project or team, or whether it affects all projects (global). An example of local scope is a code review policy which applies to one specific sub-project only. Examples of global scope are changes to this governance document, or votes outlined in the OSC Governance document.

-  **Scope: Local**
-  **Who reviews:** Readers of the affected projects’ categories on the OSC forums.
-  **Who votes:** Maintainers of the project(s), which are affected by the process, procedure, etc. are allowed to vote. This includes maintainers from incubation projects (if the scope affects the project).
-  **Scope: Global**
-  **Who reviews:** Readers of all OSC forums project categories.
-  **Who votes:** Maintainers of all active projects and the OSC Director of Community are allowed to vote.

The Director of Community first arranges a public review, followed by a timed private vote. Public review and voting should be open for a minimum of one week each. For voting a traceable poll mechanism, e.g., a voting system that keeps auditable and tamper proof records, must be used. Voting follows the conventions as laid out in "Principle: Consensus Decision Making".

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
