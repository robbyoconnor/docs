.. _trademark-policy:

Community & Project Trademark Policy
====================================

Open source software from the Open Source Center (OSC) is free to copy, to modify and to distribute. Unless otherwise indicated, it is licensed under the Mozilla Public License 2.0.

Meanwhile, the trademarks for this software – including the "Open Source Center" wordmark as well as the graphic logo – are kept as private property. Trademarks are different than copyrights, particularly in an open source community such as the Open Source Center. Trademarks can only be copied for certain specific purposes ("nominative fair use") and cannot be modified in a way to confuse consumers about the origin of the FOSS software ("confusing similarity"). These trademark law terms are described more completely below.

Examples of FOSS trademarks: Linux, Eclipse, Apache, JBoss, MySQL, Firefox, Mozilla, Jaspersoft, Hadoop, Java, OpenOffice, etc.

"Open Source Center" is like those trademarks. It is a visible brand associated with high-quality open source software. It is a brand created by our worldwide community. Our brand represents our collective pride and our reputation. We do not want anyone to misuse or misappropriate our brand. We want the value of our trademarks to accrue to the Open Source Center and its participants as a whole.

United Nations Foundation, Inc. is a non-profit corporation that owns and manages all Open Source Center-related trademarks, service marks, and graphic logos in service of our volunteer community. As a US-based corporation, we have a legal responsibility and authority to set guidelines for the use of our marks. This Trademark Policy outlines how we use our trademarks and logos to identify software developed and distributed by the Open Source Center.

The following information helps ensure our marks and logos are used in approved ways, while making it easy for the community we serve to understand the guidelines. If you have any questions about the use of logos or trademarks that are not addressed in these guidelines, feel free to contact us at osc@digitalimpactalliance.org.

Rationale for the Open Source Center Trademark Policy
-----------------------------------------------------

Open Source Center trademarks, service marks, and graphic marks are symbols of the quality and community support that people have come to associate with projects of the Open Source Center. To ensure that the use of Open Source Center marks will not lead to confusion about our software, we must control their use in association with software and related services by others. The Open Source Center and its software must be clearly distinguishable from any software from third parties, and from software or services by any company or individual that is not specifically authorized and approved by the Open Source Center. We must also prevent Open Source Center marks from being used to disparage Open Source Center software, our projects, members, sponsors, or communities, and prevent their use in any way to imply ownership, endorsement, or sponsorship of any Open Source Center-related project or initiative of any kind.

Key Trademark Principles
------------------------

This document is not intended to summarize the complex law of trademarks. It will be useful, however, to understand the following key principles:

What is a trademark?
~~~~~~~~~~~~~~~~~~~~

A trademark is a word, phrase, symbol or design, or a combination of words, phrases, symbols or designs, that identifies and distinguishes the source of the goods of one party from those of others. A service mark is the same as a trademark, except that it identifies and distinguishes the source of a service rather than a product. Throughout this policy document, the terms "trademark" and "mark" refer to both trademarks and service marks.

These rules are generalized in this document to describe Open Source Center software associated with the trademark "OSC Foo™", or more briefly "Foo™" when it is understood to refer to this specific OSC Foo software. Like all Open Source Center software, this Foo software is maintained by a Open Source Center member project.

Open Source Center trademarks are either words (e.g., "OSC" and "OSC Foo" and "Foo") or graphic logos that are intended to serve as trademarks for that Open Source Center software. The Open Source Center graphic logo is described in the Open Source Center Logo Policy, has special meaning for the Open Source Center: We intend that graphic logo to be used for linking third party websites to Open Source Center websites.

Within Open Source Center sub-projects, during our product release activity and on Open Source Center websites, we will make sure that our trademarks are marked with a (TM) or (R) symbol or shown with trademark notices where appropriate so that everyone will recognize them as Open Source Center trademarks. A current list of Open Source Center trademarks is available at our web site.

What is nominative use?
~~~~~~~~~~~~~~~~~~~~~~~

Anyone can use Open Source Center trademarks if that use of the trademark is nominative. The "nominative use" (or "nominative fair use") defense to trademark infringement is a legal doctrine that authorizes everyone (even commercial companies) to use another person's trademark as long as three requirements are met:

1. The product or service in question must be one not readily identifiable without use of the trademark (for example, it is not easy to identify Apple iPhone software without using the trademark "iPhone"); and
2. Only so much of the mark or marks may be used as is reasonably necessary to identify the product or service; and
3. The organization using the mark must do nothing that would, in conjunction with the mark, suggest sponsorship or endorsement by the trademark holder.

The trademark nominative fair use defense is intended to encourage people to refer to trademarked goods and services by using the trademark itself. This trademark defense has nothing to do with copyright fair use and should not be confused with those rules.

What is the "confusing similarity" or "likelihood of confusion" test?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some uses of another person's trademark are nominative fair use, but some uses are simply infringing. Indeed, if a trademark is used in such a way that the relevant consuming public will likely be confused or mistaken about the source of a product or service sold or provided using the mark in question, then likelihood of confusion exists and the mark has been infringed.

Note that, even if there is no likelihood of confusion, you may still be liable for using another company's trademark if you are blurring or tarnishing their mark under United States federal and/or state dilution laws, or other laws around the world.

To avoid infringing Open Source Center marks, you should verify that your use of our marks is nominative and that you are not likely to confuse software consumers that your software is the same as Open Source Center software or is endorsed by the Open Source Center. This policy is already summarized in section 2.3 of the Mozilla Public License (MPL) 2.0, and so it is a condition for your use of Open Source Center software and associated documentation:

This License does not grant any rights in the trademarks, service marks, or logos of any Contributor (except as may be necessary to comply with the notice requirements in Section 3.4).

Specific Guidelines
-------------------

The following Specific Guidelines apply to the "Open Source Center" word trademark and the Open Source Center graphic logo, as well as the trademarks and graphic logos for typical "OSC Foo" and "Foo" software produced by Open Source Center sub-projects. You may refer to our list of current Open Source Center marks at our web site.

Examples of permitted nominative fair use:

-  "Free copies of Open Source Center software under the MPL 2.0 license and support services for OSC Foo are available at my own company website."
-  "Derivative works of OSC Foo software and support services for those derivative works are available under my own trademarks - at my website." Please remember that, under trademark law, you may not apply trademarks to your derivative works of Open Source Center software that are confusingly similar to a product name such as "OSC Foo", or any of our graphic logos.
-  "Foo software is faster (or slower) than Myco software."
-  "I recommend (or don't recommend) Foo software for your business."
-  "This is the graphic logo for OSC Foo software: "
-  Using Open Source Center trademarks in book and article titles

You may write about Open Source Center software, and use our trademarks in book or article titles. You needn't ask us for permission to refer to Foo, as in "Foo for Dummies", or "Explaining Foo", or "Foo Simplified", or even "Avoiding Foo".

We prefer that you refer to "OSC Foo" rather than simply "Foo" in the title if it fits, and we request that you clearly identify that "Open Source Center", "OSC Foo", and "Foo" are trademarks of United Nations Foundation, Inc. wherever you normally acknowledge important trademarks in your book or article.

Using Open Source Center graphic logos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Graphic logos are contributed to the Open Source Center by artists as a way of creating a symbol with which Open Source Center software can be identified. Those graphic logos are special to the Open Source Center sub-projects that mark their software with those logos. The Open Source Center graphic logo is a special trademark to the Open Source Center and we intend to prevent its use in association with other companies' software or related services.

It is not necessary to ask us for permission to use Open Source Center graphic logos (the versions published on Open Source Center websites) on your own website solely as a hyperlink to Open Source Center websites, or in other materials, such as presentations and slides, solely as a means to refer to the Open Source Center itself.

All other uses of the Open Source Center graphic logo must be approved in writing by United Nations Foundation, Inc.

If you have any questions or concerns about the use of or changes to any Open Source Center graphic trademark, email us at osc@digitalimpactalliance.org. Using Open Source Center trademarks on merchandise We will typically grant written permission to apply Open Source Center trademarks (including graphic logos) for merchandise that promotes the Open Source Center, its software, community, or its worldwide mission.

Permission to apply Open Source Center trademarks will ordinarily be denied for merchandise that disparages Open Source Center software or projects or that would serve to detract from the value of the Open Source Center, its software, community, or its brands.

Using Open Source Center trademarks in domain names
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may not use Open Source Center trademarks such as "Open Source Center" or "OSC Foo" or "Foo" in your own domain names if that use would be likely to confuse a relevant consumer about the source of software or services provided through your website. You should apply the "likelihood of confusion" test described above, and please realize that the use of Open Source Center trademarks in your domain names is generally not "nominative fair use."

Using Open Source Center trademarks in relation to conferences and events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Certain Open Source Center trademarks may be reserved exclusively for official Open Source Center activities. For example, "Open Source Center Summit" may be used as our exclusive trademark for our regular Open Source Center conferences, and the Open Source Center graphic icon is intended for Open Source Center use at events in which we participate.

Individual Open Source Center sub-projects (such as "OSC Foo") may create their own conferences and events, or join with other organizations or companies to hold joint conferences or events.

The following uses of Open Source Center trademarks are probably infringing:

-  Confusingly similar software product names.
-  Software service offerings that are for anything other than official Open Source Center-distributed software.
-  Company names that may be associated in customer's minds with the Open Source Center or its trademarked project software.

Important Notes
---------------

Nothing in this Open Source Center Trademark Policy shall be interpreted to allow any third party to claim any association with the Open Source Center, United Nations Foundation, Inc., or any of its projects or to imply any approval or support by United Nations Foundation, Inc. for any third party products or services.

Open Source Center and the Open Source Center graphic logo is a trademark owned by United Nations Foundation, Inc.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
