.. _grants:

#####################
Catalytic Grantmaking
#####################

*******
Purpose
*******

We fund products that are considered public goods in the humanitarian/development space to do foundational work that is often more difficult to fund within the context of a particular project. In general, we fund projects, features or efforts that are well-aligned with the principles for digital development and the mission statement of the OSC.  The OSC will offer two rounds of calls for proposals for grants made to interventions around a particular theme, and member projects can also apply for ad hoc grants as part of a larger service offering with the OSC.

**********
The Grants
**********

In this round of thematic funding, DIAL will offer 4 grants of up to $25K to advance the mission of fostering “healthy, sustainable open-source communities and products”. While we are willing to consider all areas of project need, we anticipate these grants could be to fund high-impact areas, such as:

-  Strengthening the project’s foundational base

   -  A community coordinator to grow community contributions and
      involvement
   -  A technical writer to improve, consolidate or revise documentation
   -  Hardware infrastructure for CI, web hosting, etc.

-  Improving the project’s software quality & reliability

   -  A code spike to increase automated test coverage
   -  A code spike to refactor code smell, making the project more
      understandable for new contributors
   -  A project manager to help groom a technical backlog into tasks
      that are small, self-contained tasks that are easier for casual
      contributors

-  Tending to often-neglected but high-value areas

   -  Better packaging and training materials, to make deployments
      easier for non-technical people
   -  A marketing push, to raise awareness of the product (for both
      developers and users)

What we don’t plan to fund
==========================

-  Individuals working independently on non-collaborative projects.
-  Proprietary software projects, e.g., software not licensed under an
   OSI or FSF approved license.

All other things being equal, projects with wider scope (users, purpose, contributor/maintainer organizations) will be favored over projects with narrower scope.

Grants will be made for contributions to multi-stakeholder open source projects. Efforts on existing projects preferred, but we will consider proposals aimed at starting new multi-stakeholder projects (or features), or re-invigorating dormant ones.

Application Process
===================

Step 1: Prepare
---------------

Interested applicants are encouraged to browse through the mission statement of the OSC and also the sorts of projects we look to partner with to get a feeling for the kind of projects the program supports.

If you are not already a participating project, we request that you :ref:`start the membership proposal process <membership-details>` so we can better understand your project. Even if you are not selected for the grant, you will still gain benefits by going through this process!

Step 2: Discuss
---------------

Discuss your questions with other OSC participating members as well, and staff. In the spirit of open source, we encourage public clarification and questions, so others can learn along with you. (TODO: Add discussion media/channels.)

We encourage discussion relevant challenges within your project(s), pain points, ideas for your projects, and relevance to the goals of this round of grant funding.

Step 3: Propose
---------------

Prepare a proposal that will be sent to osc@digitalimpactalliance.org. Your proposal should be no longer than 1-2 pages, and should include:

* A proposal executive summary in narrative format,
* Budget and high-level plan of how you'd use the funds, and
* Measurable criteria of success, such as a "definition of done", milestones, or other goals.

Review Process
==============

1. Your proposal will be reviewed by the OSC Governance Advisory Board, to ensure the basic proposal criteria are included, and making sure your project is an OSC participating member. The reviewers will use the `ref:intake-evaluation` to understand where your project is currently in terms of maturity, practices, and impact, and will write a short narrative evaluation with recommendations to the DIAL funding panel.

2. The GAB will forward all reviews for this round to the funding panel, which consists of DIAL management, along with its recommendations. The

3. Depending on the funds requested and the complexity of the work to be performed, the GAB and/or the DIAL funding panel may seek independent expert review of the proposal. (For example, if the project is focused on health, feedback might be sought from members of the OSC Health Sustainability Advisory Group, or externally from PATH’s digital health team, for example. If those "external" reviews raise additional questions, grantseekers are given the opportunity to respond in writing to reviewer comments.

4. Recipients will be announced publicly at the end of the review cycle.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { topicId: 'grants',
                         discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: 'http://osc.dial.community/en/latest/grants-test.html' };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        console.log("DAVID " + window.location.href);
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
