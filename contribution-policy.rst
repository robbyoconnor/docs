.. _contribution-policy:

Contribution Policy
===================

You and other contributors are part of the growing Open Source Center (OSC). We work together to share ideas, software, documentation, bug fixes, development tools, project management resources, and anything else of value that you contribute to create useful software. Our success is solely credited to our contributors, working together as a community.

Our Contribution Policy is simple: Voluntary contributions are gratefully accepted as long as the OSC and all downstream users are allowed to use those contributions for our mission to benefit the public.

There are many kinds of contributions we invite you to make for this
cause:

-  Tangible contributions of intellectual property such as source code
   and documentation, ideas and inventions, and other technical
   solutions to problems
-  In-kind and financial donations to support our activities
-  Contributions of your time and your efforts to communicate and
   cooperate with each other in the Open Source Center.

The OSC accepts intellectual property contributions of many kinds and then collects them into software distributions that are licensed to the public under the open source Mozilla Public License 2.0. Non-code contributions such as documentation, images, or other creative works are made available under the Creative Commons Attribution 4.0 International

License. (TODO: Update based on OSC License Policy.)

The details around intellectual property contributions in particular are often tricky to understand. As described more fully below, OSC project teams review all software contributions to ensure that open source licenses for the contributions are compatible with the MPL 2.0 license.

If you have questions about any aspect of the OSC Contribution Policy, please contact osc@digitalimpactalliance.org.

Contributions of Intellectual Property
--------------------------------------

Software includes both functional and expressive works, and it sometimes arrives encumbered with many varieties of legal interests that can be owned, licensed, monopolized or sold, or limited for its use in derivative or advanced software. The OSC collects such contributed software into larger packages that we distribute as collective works under the MPL 2.0 license. To do so, we must ensure that the open source licenses applying to those contributions are compatible with MPL 2.0, and that there are no intellectual property encumbrances that would prevent the use of our collective software distribution worldwide.

As is described in the OSC Copyright and DMCA Policy, we respect all copyrights and copyright licenses.

To ensure licensing consistency, we take steps to determine that contributions of intellectual property are not encumbered. These are the steps:

1. We accept contributions only from people who identify themselves as an individual with a public account on OSC websites. Each person's profile, and all other materials he or she posts on OSC websites, is available to the public. See the OSC Privacy Policy for more information.
2. All contributions are required to be the contributor's own work licensed to the OSC under an approved open source license, or a work that is licensed to that contributor for redistribution to the OSC under an approved open source license. OSC project teams have responsibility for knowing their contributors and those contributions, as would be expected of any professional software project. Random contributions from unknown project participants without information suitable for  NOTICE file are likely not to be accepted by an OSC project.
3. OSC projects may accept ideas, suggestions, bug fixes, and other similar contributions from the public. Our standards and rules of behavior in such situations say that we should identify and acknowledge those informal and formal contributions. A NOTICE file included with each software release by the OSC will identify the provenance of all contributions, to the best of the contributors' and the project team's current knowledge and belief. Each contributor is responsible for providing enough information so that the appropriate OSC project team can prepare a correct NOTICE file.

Contents of the NOTICE File
---------------------------

The NOTICE file that accompanies every formal distribution of software by a OSC project identifies each third-party component in that software and the open source or Creative Commons license under which that component is available to the public. The following information, if available, will also be included in the NOTICE file:

-  Copyright notices supplied by the licensor(s) of any part of the software. OSC project teams may elect to remove individual copyright notices that detract from the "community" ethos of the project, but individual copyrights will still be protected by a legally-effective and encompassing copyright notice such as "Copyright (C) 2017 United Nations Foundation, Inc."
-  Patent notices identifying specific patents or patent claims that may read on the software. Contributors and all project team members are expected to disclose any patent claims of which they are aware. In the event that possible patent claims may be confidential, the contributor must disclose enough about them to alert the public about possible future encumbrances.
-  Identification of industry standards implemented by the software.
-  OSC projects and contributors may also include acknowledgement and attribution to individuals, companies or other organizations for significant portions of the software or its documentation, or who contributed in other ways to the project as a whole.
-  Other important notices that the OSC project team or its contributors want to share with the downstream users of that software.

Approved Open Source Licenses for OSC Contributions
---------------------------------------------------

The OSC relies on the recommendations of Open Source Initiative, the Free Software Foundation, and Creative Commons to determine which free and open source licenses are compatible with the Mozilla Public License 2.0 under which the OSC distributes software and documentation.

***Additional content will be developed in the coming months to provide further guidance on acceptable, preferred, and discouraged open source licenses for the community.***

Some of those licenses may not be compatible with the license requirements of some commercial companies. That is another purpose for the NOTICE file that OSC projects provide with each software distribution. Each downstream modifier and/or distributor of OSC software and documentation is responsible for making such license compatibility determinations for itself.

Rest assured that OSC software and documentation can be used for free by everyone in the world under the open source MPL 2.0 license.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
