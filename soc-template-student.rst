.. _soc-template-student:

===============================
Summer of Code Student Template
===============================

**Important:** Please make sure you've reviewed our overall :ref:`soc` documentation & your sub-org's ideas page (and any other documentation!) before starting your application.

Unless otherwise indicated, all fields on this form are required. Please contact your project mentor(s) if you have any questions, and to make sure that your application is complete after it is submitted. Ideally, a GSoC application is a **collaborative** process, so please adjust based on any feedback you are given, and start early so you can get that feedback!

***************************
Your Project Proposal Title
***************************

**Use a good project title in Google's submission system!** Your project title should explain in a few words what you plan to do and (most importantly) **always include the name of your sub-org.** Examples: "FooSys: Refactor window focusing" or "BarApp: BazPlot Integration."

The DIAL Open Source Center may get a large number of submissions in a single year, and it can be very hard for mentors to find your projects if they all have generic titles like "my gsoc application." A good title can help you get early feedback that will make you more likely to get accepted, and a bad title could result in your application getting marked as spam and ignored!

Sub-org Information
===================

* Which OSC sub-org are you applying to work with? (e.g. FooSys, BarApp)

NOTE: You MUST specify this. If we have to guess, your application could be rejected in favor of students who followed instructions. You must apply to work with a valid sub-org that is signed up with the DIAL Open Source Center to participate this year. If they're not signed up, we have no mentors for you, and will likely reject your application.

Student Information
===================

* Name:
* Alternate names: (e.g. GitHub username, IRC/chat nickname, any other preferred names.)
* Email:
* Telephone with international country code:
* Time Zone: (e.g., "East Africa Time, UTC+3".)
* Blog RSS Feed URL:

You will be **required** to blog about your GSoC experience at least every 2 weeks through the program. An RSS feed is used to aggregate student blogs; we recommend using a tag like "gsoc" if you use this blog for anything other than GSoC.

Code Sample
===========

* Link to a patch/code sample, preferably code you have already submitted to your sub-org as part of a bug/ticket/issue.

Note: The DIAL Open Source Center requires all students to submit a code contribution before being accepted. This contribution does not need to be accepted and merged into your project, but it does need to be online and available for potential mentors to inspect. Contact your mentors if you have questions about what constitutes an appropriate sample. Applications without a valid code sample will be rejected. The code sample must be an example of your own work.

Project Info
============

- Proposal Title:
  - The name of your proposal. This should explain in a few words what you plan to do and include the name of your sub-org, e.g., "FooSys: Refactor window focusing" or "BarApp: BazPlot Integration." (Including the name of your sub-org makes it much easier for mentors to find your proposal!) 
- Proposal Abstract:
  - A short description of your proposed project 
- Proposal Detailed Description/Timeline
  - Please include timeline with milestones, preferably weekly ones. You may wish to read the GSoC student guide which includes several examples of good proposals with timelines.

Note: Any pre-work such as setup and reading documentation should take place during the official GSoC Community Bonding Period, not after coding has started. Consult the official GSoC timeline for details.

Other Commitments
=================

- Do you have any other commitments during the main GSoC time period?
  - We don't penalize students for needing adjustments to schedule if they're up-front about them and have a plan to mitigate any issues. However, we *have* failed students for lying about their availability and subsequently falling behind in their work. Be honest!
- Do you have exams or classes that overlap with this period?
- Do you plan to apply for or have any other jobs or internships during this period?
  - This is highly NOT RECOMMENDED as GSoC is intended to be a full-time job, but sometimes if a student is starting or finishing an internship with a week or two overlap something can be worked out. 
- Do you have any other short term commitments during this period? (e.g., Family wedding, conference, volunteer projects, planned vacation days.) 
- Have you applied with any other organizations? If so, do you have a preferred project/org? (This will help us in the event that more than one organization decides they wish to accept your proposal.)

Optional Information
====================

This additional information isn't needed by the OSC, but can help your sub-orgs learn more about you. All fields in this extra information section are optional.

- Link to resume:
- University info
  - University Name:
  - Major:
  - Current Year and Expected Graduation date:
  - Degree (e.g. BSc, PhD): 
- Other Contact info:
  - Alternate contact info in case your primary email above stops working:
  - Homepage:
  - Instant messaging:
  - Twitter: 
- Don't forget to add any other information requested by sub-orgs here.