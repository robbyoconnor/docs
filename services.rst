.. _services:

###############################
Shared Services for OSC Members
###############################

As part of its collective action to assist its members, the OSC will make available a variety of shared services, as outlined in this document. Services may be provided through OSC leaders funded by DIAL or other organizations, through volunteers within the OSC community, or through service delivery partners. These services may be "light touch" such as advisory or mentorship work, or more "hands on" activities such as technical assistance in several areas.

Beyond this overview, see also our :ref:`current list of services <services-details>` for additional details.

*****************
Types of Services
*****************

Mentorship & Advising
=====================

At a basic level, participating projects can draw upon other members & OSC leaders for generalized advice around best practice, through our online communication channels, or participation in the various OSC governance committees and groups.

Technical Steering Committee (TSC)
----------------------------------

The OSC convenes a technical steering committee (TSC) of experts (both leaders of member projects as well as other technical experts), which is available to consult with member project leaders on questions of architecture, technology choices, and technology strategy. Participating member projects may use the TSC as a point of escalation for difficult questions, arbitration during technically-focused project conflicts, or advice for questions outside that project's current area of expertise.

Through its regular meetings & consultation services for members, the TSC may provide recommendations for standard processes, templates, technologies, and workflows to document and track work backlog & prioritization by each member project’s teams. It may also provide general 1-on-1 engineering mentoring services from the Director of Technology or other individual experts, at the request of specific member project teams or the TSC.

Should members need more "hands on" technical assistance from the TSC, they may peruse a variety of shared services based on their level of project maturity & needs, as described below.

Community Steering Committee (CSC)
----------------------------------

The OSC convenes a community steering committee (CSC) of staff and volunteers (including volunteers from member projects as well as outside resources) to serve customers & contributors of OSC member projects.

Member projects may consult with the CSC for periodic advice about marshaling resources appropriately, best practices in community management, or matching volunteers to available opportunities. The CSC also serves as an available point of escalation in conflict resolution, as well as representing member project concerns to the OSC Governance Advisory Board (GAB).

Should members need more "hands on" technical assistance from the CSC, they may peruse a variety of shared services based on their level of project maturity & needs, as described below.

Sustainability Advisory Groups
------------------------------

Through our partners, we also plan to provide **advisory services for financial sustainability planning** by & for member projects -- specifically, through our **sustainability advisory groups (SAG’s)** which will work to connect projects to funding resources within or across domains, as well as help them to prepare successful proposals for those funding opportunities. Our first such group will be a Health Sustainability Advisory Group (H-SAG), and will be available in the first quarter of 2018.

As we launch additional sustainability advisory groups for multiple sectors, these groups will also collaborate to explore areas of interoperability, technology re-use, and collaborative funding opportunities across those sectors.

Technical Assistance
====================

Mid-stage projects desiring more "hands on" services to assist with their day-to-day operations and growth often have need for various types of services that they have not been able to obtain through their project's volunteers or partners. Drawing on best practices evolved over the last 20+ years in professional open source development, OSC members will have access to several types of shared services.

Supported projects can receive direct assistance through a variety of professional service offerings, such as legal, IT infrastructure, product management, engineering management, community management, etc. These services will be provided through a combination of OSC staff resources (funded by DIAL or other partners), directly through partner resources, as well as by selected third-party service providers.

Engineering Services (via the TSC)
----------------------------------

Through the Technical Steering Committee (TSC), the OSC strives to provide ongoing tactical & strategic assistance for engineering work throughout the community's member projects. In addition to the mentorship & advising services described above, technical assistance services available through the TSC includes:

- A community-wide, full-time funded (via the OSC's fiscal sponsor) Director of Technology available to assist member project leaders in clarifying and documenting a technical architecture for each project,
- Available advanced-tier technical support & consultation for member projects engineering challenges, through the Director of Technology or other roles,
- Monitoring of member project’s work backlog to ensure there are ample opportunities for potential volunteers, and
- Hands-on architecting and coding assistance from the Director of Technology or other individuals, at request of specific member project teams.

Community Services (via the CSC)
--------------------------------

As the convener of a Community Steering Committee (CSC) serving customers & contributors of OSC member projects, the Center can provide the following services to the community to help maintain itself in a sustainable way:

- Assistance with governance planning to create and refine project processes, policies, and standards,
- Preparation for (and application to) 3rd party fiscal sponsors, such as OSC partner Software Freedom Conservancy,
- Organization of and participation in external outreach & recruitment programs, such as Outreachy, Google Summer of Code, and Google Code-in,
- Assistance of member projects in making of community-wide non-technical decisions,
- Organization of project marketing & outreach programs, and
- Assistance with volunteer management for newcomers interested in contributing to one or more member projects.

Legal Services
--------------

Through our partner Software Freedom Conservancy, OSC members may be able to obtain access to some or all of the following:

-  Trademark ownership for member projects’ assets,
-  Copyright assignments of distributed packages of FOSS-licensed software,
-  Limited liability protection for contributors & leaders,
-  Bank accounts for holding financial assets,
-  Appropriate legal response to DMCA and other copyright-related claims, and
-  Appropriate tax compliance, operating as a public charity or other similar non-profit organization.

Infrastructure Services
-----------------------

Additionally, as membership demand warrants, the OSC will secure and/or operate various infrastructural services commonly used by well-functioning FOSS projects such as:

-  A globally-accessible forge with public and private code repositories,
-  Web site hosting (project pages or sites as size may warrant),
-  Issue trackers for community-level work and member project work,
-  Forums and/or mailing lists as appropriate for overall community strategy,
-  Real-time chat service for contributors to projects and for customers,
-  Wikis and/or other platforms for written documentation on software as well as participation processes of each member project,
-  Continuous integration/delivery pipeline tools for unit & integration testing, as well as deployment purposes,
-  Non-production deployment targets for various interactive testing purposes by developers/contributors, automated test systems, user acceptance testing, and public “demonstration” purposes,
-  Monitoring tools to ensure a high level of availability of these systems, and
-  Coordination of a team of project volunteers to help manage these services & consult in their effective use.

Documentation Services
----------------------

The OSC plans to forge partnerships with a community of "documentarians" interested in the T4D field, to be available on a shared basis across multiple member projects. These individuals would generally focus on documentation strategy, as well as creating frameworks, tools, and processes to increase the capacity for quality documentation for your project's users and contributors.

More details about documentation services will be made available later in 2017.

Direct Grantmaking
==================

The Center will offer projects small tactical grants ($50k to $100k) to address neglected bottlenecks affecting project effectiveness and momentum.
The grants may target any of the challenges facing projects including product issues (design, reliability, packaging, documentation), organizational
(governance, legal), infrastructure, strategy, etc.  Grants can be applied for on an ad hoc basis as part of a larger service offering proposal,
or projects can apply to biannual thematic grants for specific, focused interventions. :ref:`Read more details about our grantmaking services. <grants>`

Potential Future Services
=========================

As the OSC grows, we plan to offer related shared management services assist with organization of members' business affairs. These services may include:

-  Corporate compliance, such as convening meetings of a board of directors for the host organization, as well as filing of necessary reports and paperwork,
-  Ongoing accounting of donations and other revenue, disbursement of funds in accordance with direction from the advisory board, and related bookkeeping tasks,
-  Preparation and submission of various tax filings, and
-  Publication of necessary documentation to ensure transparency of management operations.
-  Business development and fundraising for individual member projects (as well as the community as a whole),
-  Development and operation of of an appropriate funding/compensation model for individual contributors & volunteers to take on task such as feature development work, business analysis, and other advanced engineering management roles,
-  Marketing & advertising of member projects in various channels.

By participating in (or providing feedback to) the OSC Governance Advisory Board, members can influence the availability of new services and growth of the Center.

Service Details
===============

Read our :ref:`full list of services <services-details>` for additional details.

*********************************
Service Benefits for Stakeholders
*********************************

Through the delivery of these services, the OSC impacts three key constituencies:

1. **Funders and sponsors** of participating projects or the OSC initiative as a whole.
2. Participating **project contributors and maintainers** working to plan, build, and grow those projects.
3. **Consumers of participating projects** such as implementing organizations and end users.

The following chart explores some of the services we offer, and which of these groups can benefit.

+--------------------------------------------+---------+----------+-----------+
| Benefit                                    | Funders | Projects | Consumers |
+============================================+=========+==========+===========+
| Operation of a 3rd-party, neutral “home”   |    ✓    |    ✓     |     ✓     |
| for participating projects to minimizes    |         |          |           |
| conflicts of interest, reduce single points|         |          |           |
| of failure, and improve trustworthiness    |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Vetting of mentored & member projects      |    ✓    |    ✓     |     ✓     |
| by a Technical Steering Committee (TSC) of |         |          |           |
| industry experts & peers                   |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Project mentoring program that maintains   |    ✓    |    ✓     |     ✓     |
| participating project autonomy while       |         |          |           |
| adding best practices and industry         |         |          |           |
| expertise to increase their maturity       |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Professional management services shared    |    ✓    |    ✓     |     ✓     |
| across participating projects provide a    |         |          |           |
| cost-effective way to handle common        |         |          |           |
| infrastructure as well as engineering &    |         |          |           |
| community management needs                 |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Operation of multi-project & cross-sector  |    ✓    |    ✓     |     ✓     |
| community work groups for key areas of     |         |          |           |
| need like documentation, software quality  |         |          |           |
| best practices, diversity & inclusion,     |         |          |           |
| and more                                   |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Priority participation in DIAL’s proposed  |    ✓    |    ✓     |     ✓     |
| Deliver ICT concept for capacity building  |         |          |           |
| & training, connecting open source digital |         |          |           |
| development projects with real-world       |         |          |           |
| implementation work                        |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Short-term tactical funding opportunities  |    ✓    |    ✓     |           |
| for high-impact areas of need, via DIAL &  |         |          |           |
| partners                                   |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Facilitation in matching participating     |    ✓    |    ✓     |           |
| projects to more longer-term funding       |         |          |           |
| opportunities through Sustainability       |         |          |           |
| Advisory Groups (SAGs)                     |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Marketing & advertising work of            |    ✓    |    ✓     |           |
| participating projects through inclusion   |         |          |           |
| in DIAL and the Center talks at global     |         |          |           |
| digital development & open source events   |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Operation of a collaborative community,    |    ✓    |    ✓     |           |
| co-investing in common infrastructure &    |         |          |           |
| services, to achieve economies of scale in |         |          |           |
| foundational project support               |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Close partnership with the Principles of   |         |    ✓     |     ✓     |
| Digital Development community of           |         |          |           |
| implementers and practitioners to improve  |         |          |           |
| collaboration with consumers & adopt       |         |          |           |
| project best practices                     |         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Open source project event planning best    |         |    ✓     |     ✓     |
| practices, toolkits, & assistance/resources|         |          |           |
+--------------------------------------------+---------+----------+-----------+
| Available add-on professional services on  |         |    ✓     |     ✓     |
| ad-hoc basis for legal, communication,     |         |          |           |
| technical architecture, events, monitoring |         |          |           |
| & evaluation, etc.                         |         |          |           |
+--------------------------------------------+---------+----------+-----------+

*********************
Applying for Services
*********************

Step 1: Become a member
=======================

If you are interested in accessing the OSC's shared services for your project, the first step is to become a member. Browse through the mission statement of the OSC and also the sorts of projects we look to partner with, to get a feeling for the kind of projects the program supports. If you think your project might be a fit, :ref:`start the membership proposal process <membership-details>` so we can better understand your project. Even if we don't ultimately find a beneficial service offering, you will still gain benefits by going through this process!

Step 2: Identify needed services
================================

Contact the OSC GAB by email at osc@digitalimpactalliance.org. Let us know which project you're working with and what type of services sound correct for your project. We will request more information to follow up, help your project understand available resources, any associated costs, and discuss additional details.

Step 3: Submit a proposal
=========================

The GAB will ask your project leadership to submit a formal request for services document, which will be reviewed by the GAB in an upcoming meeting, and scheduled based on available capacity. The GAB will conduct a preliminary evaluation of the project using the :ref:`intake-evaluation` document, which can be used to track the project's growth over time. Your project will be involved at every step to ensure there are no surprises, and to make sure the plans work with your project's needs.

Services are generally prioritized based upon how they will affect your project's increased maturity & impact to those served.

.. raw:: html

    <div id="discourse-comments"></div>
    <script type="text/javascript">
      DiscourseEmbed = { discourseUrl: 'http://forum.osc.dial.community/',
                         discourseEmbedUrl: window.location.href };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    </script>
    
