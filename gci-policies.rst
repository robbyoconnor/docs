.. _gci-policies:

========================================
Google Code-in @ DIAL Open Source Center
========================================

The `Digital Impact Alliance (DIAL) <http://digitalimpactalliance.org/>`_ at `United Nations Foundation <http://unfoundation.org/>`_ has launched its `Open Source Center <http://www.osc.dial.community/>`_ to provide a collaborative space for (and professional technical assistance to) open source projects focusing on international development and humanitarian response. The Center assists in the establishment of effective governance, software project management, and contribution models for member projects. It also provides technical, architectural, and programming support for projects; and assists those projects in support, engagement, management of their communities of contributors and implementers.

`Google Code-in (GCI) <http://codein.withgoogle.com/>`_ is a global, online contest introducing pre-university students ages 13 to 17 to the world of open source development. With a wide variety of bite-sized tasks, it’s easy for beginners to jump in and get started no matter what skills they have. Mentors from the DIAL Open Source Center’s participating organizations lend a helping hand as participants learn what it’s like to work on an open source project. Participants get to work on real software and win prizes from t-shirts to a trip to Google HQ!

The DIAL Open Source Center (OSC) serves as an "umbrella organization" to a variety of international development and humanitarian response related projects, to help raise awareness about those projects among student applicants, connect students to meaningful open source work that makes the world a better place, and help increase the maturity & reach of those projects. The 2018 OSC coordinator for GCI is Michael Downey, the OSC's Director of Community. (downey on Freenode IRC, downey (at) dial (dot) community, but please email osc (at) dial (dot) community if you are a mentor who wishes to contact an admin. Students should almost always visit Getting Started first, and then email osc (at) dial (dot) community only if you get stuck.

**We been accepted to participate in GCI as an umbrella org for the 2018 season!** If you haven't already done so, check out our sub-orgs and affiliates listed below, and get in touch with them to learn more about their projects. Follow the documentation on this page, and the general GCI web site, for more information.

.. _gci-sub-orgs:

**********************
2018 Sub-Organizations
**********************

**CURRENT STATUS: Time to get in touch with sub-orgs & mentors!** Our sub-orgs are actively developing their task lists. See the sub-org lists below for details. This section contains information about sub-orgs and their task lists once they have gotten in touch with the OSC.

LibreHealth
===========

.. image:: 2018-librehealth.png
    :alt: LibreHealth
    :width: 200

LibreHealth is collaborative community for free & open source software projects in Health IT. The project's software is driven by real needs of patients and last-mile clinicians who want to improve health and health service delivery.

* Project website: http://librehealth.io/
* Forum: https://forums.librehealth.io/c/community/gci
* Chat: https://chat.librehealth.io
* **Task list:** TBA


Mifos
=====

.. image:: 2018-mifos.jpg
    :alt: Mifos
    :width: 200

Mifos is a community of financial service providers, technology specialists, financial services experts, and open source developers working together to grow an open source platform for financial services.

* Project website: http://mifos.org/
* Mailing lists: http://mifos.org/resources/community/communications/
* Chat: https://gitter.im/openMF/mifos
* **Task list:** https://mifosforge.jira.com/wiki/x/iADuB


Ushahidi
========

.. image:: 2018-ushahidi.jpg
    :alt: Ushahidi
    :width: 200

Ushahidi helps marginalized people raise their voice and those who serve them to listen and respond better. It allows for custom survey creation, and the running of multiple surveys on a single deployment, amongst other feature improvements from v2 such as embeddable maps and surveys, analytics, private deployments, and management of roles and permissions. It is built on the Laravel PHP web framework.

* Project website: https://www.ushahidi.com/
* Forum: http://forums.ushahidi.com/
* Mailing list: http://list.ushahidi.com/
* IRC chat: http://irc//irc.freenode.net/#ushahidi
* **Task list:** TBA


OSC Members Participating as Independent Projects
=================================================

OSC member projects may also take part in GCI as "standalone" organizations this year, after having participated during previous years. These open source projects are not operating under the OSC umbrella, but they also serve international development and humanitarian response. Please visit their project ideas pages too!

Public Lab
----------

.. image:: 2018-publiclab.png
    :alt: PublicLab
    :width: 200

Public Lab is a community where you can learn how to investigate environmental concerns. Using inexpensive do-it-yourself techniques, they seek to change how people see the world in environmental, social, and political terms. The community collaborates to invent and improve open source environmental science tools.

* Project website: https://publiclab.org/
* Forum: https://publiclab.org/wiki/public-lab-q-and-a
* Chat: https://chat.publiclab.org/
* **Task list:** TBA


.. _gci-students:

************************
Information for Students
************************

Make sure you've read all of the program information at https://codein.withgoogle.com/. Remember that Google intends this to be a way to introduce young people to the world of open source. The students most likely to be selected as grand prize winners are those who are engaged with the community, and those who indicate that they are hoping to continue their involvement for more than just the duration of the program.

To participate, you need to take a look at the participating organizations and the tasks lists that they publish. Sometimes, projects are open to new task ideas from students, but if you propose something new, make especially sure that you work with a mentor to make sure it's a good fit for your community. Unsolicited, un-discussed task ideas are less likely to get accepted.

**Note that the DIAL Open Source Center (OSC) is an "umbrella organization" which means that our team is actually a group of projects that work together to do Google Code-in.** As a student participant, you'll need to choose from one of those teams, because that defines which tasks you'll work on, and which mentors will be helping you. You can work with more than one sub-org during the contest, and all of those sub-orgs will count for your credit with Digital Impact Alliance. Here's some resources so you can read up more on how to be an awesome student:

- `The How GCI Works page <https://developers.google.com/open-source/gci/how-it-works>`_ -- This is a guide with contributions by mentors and former students. It covers many questions that most students ask us. Please read it before asking any questions if you can!
- `Google's list of resources <https://developers.google.com/open-source/gsoc/resources/>`_ -- Note especially the `Frequently Asked Questions (FAQ) <https://developers.google.com/open-source/gci/faq>`_ which does in fact answer 99% of the questions students ask.
- Our OSC Google Code-in :ref:`gci-faq` will answer the questions that we most often get about OSC projects. You might want to see "How do I choose a project or a sub-org?" for example.

*************************************
Students: About plagiarism and re-use
*************************************

**Anything a student creates for any of our GCI tasks must be that student's own original work, or used with permission. Students must read and follow the instructions below if they plan to include or modify someone else's work for this (or any) task.**

If you're not familiar with the term "plagiarism", or even if you are, you should read: https://www.plagiarism.org/article/what-is-plagiarism

Google, the DIAL Open Source Center, and all of our sub-orgs have a **zero-tolerance policy** towards any form of plagiarism. If it is detected by any party, you may be unassigned from a task, and may be reported to Google Code-in program administrators. In severe cases, this may lead to you being disqualified from the program.

Any work you create (code, artwork, documents, etc.) for any task must be **something original** work and it must be something **you** created. Any elements (other code, clip art, images, words, videos, etc.) that you use in creating the work for your task **must either have an Open Source or Creative Commons license.** You can `search for Creative Commons materials to use in your work. <https://ccsearch.creativecommons.org>`_ You must `attribute (give credit to) the original author. <https://creativecommons.org/use-remix/get-permission/>`_ If you don't have this type of permission, whatever you submit must be entirely your own **original work**. 

**Separately to whatever you submit or attach for your task(s), you must include any of these attributions, which can be obtained using the search tool for Creative Commons mentioned above.**

If you are unsure how to use someone else's work in your task, such as using someone else's image, or quoting someone ... please ask a mentor, sub-org admin, or DIAL staff. We are happy to help you learn!

TLDR: If you did not produce the work, and if you did not provide attribution, your submission may be considered plagiarism. GCI requires all work to be original, and in your own words if it is written work.

.. _gci-dates:

***********************
Important Program Dates
***********************

**Next Deadline:** **Tuesday, October 23 (17:00 UTC):** Contest opens for entries by student participants.

Current Key Dates:
------------------

* **Tuesday, October 23 (17:00 UTC):** Contest opens for entries by student participants.
* **Monday, December 10 (17:00 UTC):** Deadline to claim new tasks.
* **Wednesday, December 12 (17:00 UTC):** All student work must be submitted; contest ends.
* **Thursday, December 13 (17:00 UTC):** Mentoring organizations complete all evaluations of students’ work.
* **Thursday, December 27 (17:00 UTC):** Mentoring organizations submit list of Winners and Finalists.

Please note `Google's official GCI dates and deadlines <https://developers.google.com/open-source/gci/timeline>`_. It is the final word when it comes to any deadlines.


.. _gci-suborg:

***********************************
Participating as a Sub-Organization
***********************************

First of all, make sure you understand what's involved and the benefits of participating in Google Code-in. Review all documentation first, and drop us a line at osc (at) dial (dot) community if you have questions!

To participate under the DIAL Open Source Center (OSC) umbrella, a sub-organization must do the following:

#. Be an open source project that is focused on international development, humanitarian response, or other such project that is aligned to focus on `the DIAL Open Source Center challenge <http://www.osc.dial.community/challenge.html>`_.
#. Have one sub-org admin and at least two mentors who are willing to commit to the full GCI period. (The admin can also be a mentor, but the more people, the better!)
#. All sub-org admins and mentors agree to follow the :ref:`OSC Code of Conduct <code-of-conduct>` for the duration of the program.
#. Be able to develop and maintain a good queue of tasks throughout the program. See :ref:`gci-template-suborg` for an example. Getting a really great page sometimes takes a few rounds of revisions. We will work with you to make sure your page is ready! Once you're ready for review, you can send request to be added to this page.
#. Be able to handle meeting deadlines and following both Google and OSC's rules. We try to send important reminders for big deadlines, but we only have limited volunteer time for nagging and cajoling. Groups that cause repeated problems may be asked to take time off to limit volunteer burnout.

We can't promise to take everyone who meets those criteria, but we do try to take any eligible project/organization that we feel will give the students a great experience. The OSC Director of Community has final say in which projects participate under the OSC umbrella, but please send any queries to the team at osc (at) dial (dot) community to make sure we're all on the same page.

T4D, HFOSS, and other international development focused projects are welcome and encouraged to apply as separate mentoring organizations directly with Google. We're happy to help you in any way we can and we don't mind being your backup plan. We're also happy to help advertise related organizations not under our umbrella -- we want students to find projects that best suit them!

.. _gci-cfm:

Mentors Wanted!
===============

Interested in volunteering with the DIAL Open Source Center or one of our participating projects? First and foremost, please review Google's list of GCI mentor responsibilities at https://developers.google.com/open-source/gci/help/responsibilities#mentor_responsibilities.

**The biggest job is mentoring students:** Mentoring students can be a pretty big time commitment. We recommend around 0-10 hours a week on average for the duration of the program, with more time at the beginning and less once students learn to work more independently. It's a very rewarding chance to give students open source training opportunities. Mentorship happens in teams, so even if all you can handle is a few code reviews or taking over for a week while someone's on vacation, our projects still love your help.

**The easiest way to become a mentor is to be part of one of the sub-orgs that plan to be involved, so get in touch with them directly if you want to help.** If you're part of a group that would like to participate as a sub-org, please read the section for sub-orgs below.

Google produced a great video with advice on being a great mentor for their larger Google Summer of Code (GSoC) program. While the program is geared more for long-term student projects, many of the ideas apply to GCI too. Take a look:

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3J_eBuYxcyg?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

But our projects also need other volunteers! We're also looking for friendly community members to help with other tasks! We'd love to have more people available on IRC, mailing lists, and forums to answer student and mentor questions in various time zones. We are particularly looking for volunteers who can promote the work our students do to the larger open source & international development community. Or maybe you have another skill set you'd like to contribute? (Proofreading? Recruiting diverse student applicants?) If you want to help, we'd be happy to find a way to make that happen!

If you'd like to volunteer, get in touch with a sub-org admin or email our GCI team at admins at osc (at) dial (dot) community.

.. _gci-faq:

********************************
Frequently Asked Quetsions (FAQ)
********************************

Getting Started
===============

How do I get started in open source?
------------------------------------

Generally speaking, and above and beyond participoating in GCI, here are the top things you can do to get started in free and open source software:

#. Choose an organization to work with.
    - There are thousands of open source software projects, and the list of projects focused on international development is also long. You need to narrow down the list before you can get help or do much that's useful. See "How do I choose a project or sub-org?" for ideas on how to do that.
    - Any open source experience can help you prepare for GCI, so don't worry too much about what project you try first and don't be afraid to change your mind!
    - For GCI participation, you'll need to choose from the list of accepted OSC sub-orgs, or Google's master list of participating orgs. If your favorite open source project isn't on the list, contact them to see if they're interested in participating next time.

#. **Set up your own development or design environment.** This will vary depending on what sub-org(s) you are interested in, and what type of tasks you want tow ork on, but each sub-org and task description should have documentation and help for you along the way.
    - Be sure to document what you do, so you can remember it later, and so you can help others if they get stuck! And if you get stuck, don't be afraid to ask for help.

#. **Start communicating with a sub-org's project contributors.** Join the mailing list, chat channels, or any other communication channels the developers use. Listen, get to know the people involved, and ask questions.
    - In almost all cases, you should **communicate in public** rather than in private. GCI is a busy time for many contributors, and many beginner questions get asked repeatedly. Help keep your mentors less stressed out, by reading what they've written already, and making it easier for them to have a record of the things they've answered. You can use a pseudonym/nickname if you want. Also, search those archives to make sure you're not asking something that's just been asked by someone else!
    - If you want to make the best first impressions, DO NOT start with "Dear Sir." and DO NOT ask to ask. See our :ref:`gci-faq` for details.

#. **When the contest starts, begin with beginner-friendly tasks.** Many projects have these tagged as "beginner-friendly" so try searching for those terms or looking at the tags to figure out which bugs might be good for you. Sub-org project ideas page will point you to them.
    - Remember, competition for beginner tasks is very high during GCI, so it can be hard to find one that's tagged. If you don't see anything from your search, read through the tasks and see if any others sound like something you can fix. Remember to ask for help if you get stuck for too long, "I'm a new contributor and was trying to work on task X. I have a question about how to..." -- if people can't help, sometimes they will be able to suggest another task which would be more beginner-suitable.

#. **Find bugs and report them.**
    - Hopefully you won't encounter too many, but it's always a good idea to get familiar with your project's bug reporting process.

#. **Help with documentation.** As a beginner in your project, you're going to see things that are confusing that more experienced developers may not notice.
    - Take advantage of your beginner mindset and make sure to document anything you think is missing!

#. **Help others.** This is a great idea for a lot of reasons:
    - Explaining things can help you learn them better.
    - Demonstrating your skills as a good community member can make you more memorable when your mentors have to choose candidates.
    - Being helpful makes your community a better place!

How do I choose a project or sub-org?
-------------------------------------

Choosing a project is a pretty personal choice. You should choose something you want to work on, and none of us can tell you exactly what that would be! But here's a few questions you might want to ask yourself to help figure that out:

* **How do you want to change the world?** Do you want to help people learn more? Communicate better? Understand our world better? With the DIAL Open Source Center, all of our projects are designed to help improve communities and peoples' lives, so you'll have choices!
* **What would you like to learn?** GCI is meant to be a bit of a learning opportunity. Have you always wanted to be more involved with health? Data? Visualization? Education? See which projects might help you improve your skills.
* **Who do you like working with?** Hang out where each project's contributors do, and get to know some of your potential mentors. Which developers inspire you?
* **How do you like to communicate?** Do you like realtime chat communication? Perhaps you should choose a project with mentors in a time zone close to you. Do you like asynchronous communication on mailing lists or forums? Find a group with active discussions. Communication is a big part of Google Code-in (and really any open source development in a team!) so finding a team that works the way you want to work can make your experience more awesome.

A list of sub-orgs for this year will be published on this page.

**In most open source projects, you're going to be encouraged to make some decisions on your own. You can make a better first impression on mentors by showing that you're able to narrow down your field of choices!**

What do I need to know to participate in Google Code-in with OSC?
-----------------------------------------------------------------

The answer to this depends a lot on the sub-org and tasks you choose. Each sub-org should maintain a range of tasks, from beginner to advanced. Every sub org expects different things from their students. Maybe you'll need to know a bit about machine learning, graphic design, email, or image processing. The best answer to this question is, "always ask your mentors what you will need to know" for a specific task, if it's not clear by reading it.

But a lot of people ask early on because they want to be sort of generically ready, but they're not sure what they want to do yet. So the above answer is not always super helpful.

In that case, here's a list of a few things that are useful for most OSC projects:

* **For coding tasks, you should have some experience with software development.** You can be a beginner, but practicing in advance is good! And there are a lot more projects available for students who are reasonably used to a project's specific language, so more practice means you'll have more project options.
* **You need to feel comfortable asking questions,** because we're going to expect you to ask if you don't understand something.
* **You should be comfortable communicating your ideas to others in public.** Most projects have public mailing lists or forums, and would prefer if you use them. You can use a pseudonym (nickname) if that works best for you. Google will need to know who you are in order to participate in the contest, but we just need something to call you.
* **You probably want some experience with version control.** While this varies depending on the type of task you're doing, we have a lot of projects that use different tools, such as Git, Mercurial, or Subversion, and you can find out which one your project uses in advance and practice using it on your schoolwork or personal projects to get used to it.

Communication
=============

What does "don't ask to ask" mean?
----------------------------------

You'll hear this phrase sometimes on IRC or other chat systems, and it means, "Please just ask your question, don't say something like 'can I ask a question?' first."

Why? Developers are often pretty busy, and if you just ask the question, someone can jump in the minute they see your message with the answer or direct you to folk who can answer it better.

If you ask "can I ask a question?" you're basically just waiting for someone to say "yes" before any useful information is communicated. Many folk consider this slow, annoying, and perhaps even rude. Save everyone (including yourself!) some time and just ask the question right from the start. Culturally speaking, in open source projects it's generally ok launch right in to a question on chat; you don't even have to say hi first!

What should I do if no one answers my question?
-----------------------------------------------

* Be patient. If you're on Zulip or another chat tool, stick around for an hour or so (you can do something else, just leave the window open and check back occasionally) and see if someone gets back to you. If they don't, try posting to the forum or mailing list. (It's possible all the developers are asleep!) You should give people around 24-36h to answer before worrying too much about it.
* Make sure you're asking in the best place. One common mistake students make is to contact individual developers rather than asking on a public mailing list or a public Zulip/chat channel. You want as many people as possible to see your questions, so try not to be shy! (Don't worry about looking too much like a newbie -- all of us were new once!) Sometimes projects have different lists/chat channels/forums/bug queues for different types of questions. If you're not sure, do feel free to follow up your question with something like, "Hey, I haven't gotten an answer on this. Is there somewhere better I could post it or more information you need to help?"
* Try giving more information. If you've hit a bug, try to give the error message and information about your setup and information about what you've already tried. If you're trying to find a bit of documentation, indicate where you've already looked. And again, "Hey, I haven't got an answer; what other information could I provide to help debug this problem?" is usually a reasonable follow-up if you're not sure what people might need.
* If you're really having trouble getting in touch with mentors, talk to the OSC team emailing osc (at) dial (dot) community. The GCI org admins should have contact info for mentors with each project and can help connect you. (Note: Please don't complain that you can't get in touch with us on the general Google lists or the global #gsoc IRC channel. They're just going to redirect you to the OSC org admins anyhow!)

How should I address my emails or forum posts?
----------------------------------------------

*(Or, "Why shouldn't I start with 'Dear Sir'?")*

If you want to make the best first impression, **do not start emails with "Dear Sir."** OSC has many mentors who are female and/or prefer other forms of address. We realize you're trying to be polite, but "Dear Sir" is often perceived in our communities as alienating, rude or simply too formal and off-putting.

Try "Dear developers" or "Dear mentors" if you're sending a general message. If you're addressing a specific person, use the name or nickname that they use on their emails. Culturally speaking, first names or chosen nicknames are fine for most open source projects. Don't worry about formality, open-source organizations do not require such formality.

How will you select the Grand Prize winners and finalists beyond task count?
----------------------------------------------------------------------------

We are working on deploying a tool that will quantify community interaction (forums, mailing lists, chats, other non-GCI issues, etc.) and will have a neutral algorithm to take all of that into account. From there, our plan is to ask each sub-org admin to prepare a "pitch" for any of their students in the top 6, and take that to the DIAL OSC governance board (who don't represent any of the participating projects) for evaluation & voting.

Mentoring
=========

What does it take to be a mentor?
---------------------------------

* Mentors for Google Code-in need to be vetted thoroughly -- you don't need to do background checks but they need to have a track record with your project prior to Google Code-in.
* Google Code-in is a lot of work since unlike Google Summer of Code, experience is not guaranteed -- it might be non-existent in most cases. This may be their first exposure to the FOSS world.
* The time commitment will depend largely on how many mentors your sub-org has on hand. Ensure that there is adequate coverage across time-zones. Don't worry about having 24/7 coverage, but ensure students get their questions answered within 24h to 36h.
* Patience. Seriously -- sometimes it can get overwhelming and frustrating but remember, they're kids. You didn't know everything at their age. The quality of education varies in some parts of the world. Keep this in mind.
* We want at least two mentors per project, so hopefully no one ever gets overwhelmed and feels like they're always on call. Google does ask that we try to answer questions within 24h to 36h so students can't get stuck for too long. Remember, no one mentor has to know all the answers.
* Mentors don't have to be the "Best At Everything". A lot of what mentors do is keep students on track and keep them from getting stuck for too long. Sometimes that means just knowing who to ask or where to look rather than knowing the answer yourself. In an ideal world, at least one mentor can answer at least basic architectural questions and knows how to get code accepted upstream, though.

Task Guidelines
=====

GCI has the following task categories. Each task for every sub-org must fit into one of the following:

* Coding
* Documentation and Training
* Outreach and Research
* Quality Assurance
* Design

Guidelines for sub-org tasks:

* Be concise with your instructions. Don't make students read pages of instructions.
* Maintain a variety of coding and non-coding tasks throughout the program.
* Design enough beginner tasks that are easy for newcomers. Ensure there are always enough beginner tasks available.
* Specify which category your task is meant to be in, and whether or not it is an introductory task by adding (I) to the task name at the end.
* Think like a 13-year-old kid, and ask whether or not you'd understand the task instructions. If they cannot understand them, they will move on and your sub-org will lose potential contributors.
* Specify if each task is intended to be completed multiple times by students, once by a single student, or once per student. Sometimes this is obvious, but please state it clearly.

Each task MUST include:

* Resources and hints,
* How to get started,
* How to do the task,
* How to submit the task (GitHub, GitLab, email, etc.), and
* How a student should ask for help if they get stuck.

.. _gci-contact:

****************
Getting in Touch
****************

* Please note that :ref:`the OSC has a Community Code of Conduct <code-of-conduct>` and mentors and students working with the OSC must abide by it as members of the community.
* Sign up to our forum at https://forum.osc.dial.community/ list to get updates, reminders, and to discuss questions.
* https://dial.zulipchat.com is our real-time chat service. Zulip is an open source project similar to IRC or Slack. Please visit the "#OSC Google Code-in" channel with questions or ideas!
* Found a typo or want to improve this page? Use the "Edit on GitLab" link above and submit a merge request!
* To get in touch with people from a specific sub-org, check their individual pages, or reach out through the OSC Zulip Chat.

Please try to read all the information on this page before asking a question. We have tried to answer a lot of common questions in advance!

**Remember to be patient:** Our mentors generally have day-jobs and are not always paying attention to chat, mailing lists, or forums. (This is especially true during GSoC off-season. Expect more active mentors after Google's announcement of organizations.) Please ask questions directly on channel (you don't need to introduce yourself or say hi first) and please be patient while waiting for an answer. You could wind up waiting an hour or much longer for "real-time" answers if all the mentors are in meetings at work or otherwise occupied. If you can't stay that long, stay as long as you can and then post to the forum instead so mentors have some way to reach you. We try to answer questions within 24h. Remember, finalists and grand prize winners will be based not just on the number of tasks you complete, but the **quality** of interactions sub-orgs have had with you during the contest. Numbers are important, but not everything. **QUALITY** over **QUANTITY**. Rushing through tasks won't help you win.

**For mentors:** The OSC GCI team can be reached at osc (at) dial (dot) community if you or students have questions about participating. You can also contact the team throught the mentors channel on the DIAL Zulip chat.

.. _gci-ack:

****************
Acknowledgements
****************

This work was adapted from LibreHealth task guidelines by Robby O'Connor, and "Google Summer of Code 2018 @ the Python Software Foundation" by Python Software Foundation, used under the Creative Commons 4.0 International Attribution License (CC BY 4.0). 

Our umbrella org strategy for Google Summer of Code and Google Code-in has also been adapted from the time-tested policies, processes, and content from the excellent PSF team, who have provided mentorship and advice to our umbrella program. Thank you!
